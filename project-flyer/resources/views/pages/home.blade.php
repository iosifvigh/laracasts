@extends('layout')

@section('content')

    <div class="jumbotron">
        <h1>Project Flyer</h1>
        <p>Laracasts example project</p>

        @if (Auth::check())
            <a class="btn btn-primary" href="/flyers/create" type="button">Create Flyer</a>
        @else
            <a class="btn btn-primary" href="/auth/register">Create Flyer</a>
        @endif
    </div>

@stop