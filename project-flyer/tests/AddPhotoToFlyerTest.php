<?php
namespace App;

use Mockery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TestCase;

class AddPhotoToFlyerTest extends TestCase
{
/** @test */
    public function it_processes_a_form_to_add_a_photo_to_a_flyer()
    {
        $flyer = factory(Flyer::class)->create();
        $file  = Mockery::mock(UploadedFile::class, [
            'getClientOriginalName'      => 'foo',
            'getClientOriginalExtension' => 'jpg',
        ]);

        $file->shouldReceive('move')
             ->once()
             ->with('storage/photos', 'nowfoo.jpg');

        $thumbnail = Mockery::mock(Thumbnail::class);
        $thumbnail->shouldReceive('move')
                  ->once()
                  ->with('storage/photos/nowfoo.jpg', 'storage/photos/th-nowfoo.jpg');


        $form = new AddPhotoToFlyer($flyer, $file, $thumbnail);

        $form->save();

        $this->assertCount(1, $flyer->photos);
    }


    function time()
    {
        return 'now';
    }

    function sha1($path)
    {
        return $path;
    }
}
