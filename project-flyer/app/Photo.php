<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class Photo extends Model
{
    protected $table = 'flyer_photos';

    protected $fillable = ['path', 'name', 'thumbnail_path'];


    /**
     * A photo belongs to a flyer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function flyer()
    {
        return $this->belongsTo('App\Flyer');
    }

    public function baseDir()
    {
        return 'storage/photos';
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->path               = $this->baseDir() . '/' . $name;
        $this->thumbnail_path     = $this->baseDir() . '/tn-' . $name;
    }

    public function delete()
    {
        File::delete([
            $this->path,
            $this->thumbnail_path
        ]);

        $parent::delete();
    }
}
