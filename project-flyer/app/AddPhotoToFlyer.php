<?php

namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AddPhotoToFlyer
 * @package App
 */
class AddPhotoToFlyer
{
    /**
     * @var Flyer
     */
    protected $flyer;
    /**
     * @var UploadedFile
     */
    protected $file;
    /**
     * @var Thumbnail
     */
    protected $thumbnail;


    /**
     * AddPhotoToFlyer constructor.
     *
     * @param Flyer        $flyer
     * @param UploadedFile $photo
     * @param Thumbnail    $thumbnail
     */
    public function __construct(Flyer $flyer, UploadedFile $photo, Thumbnail $thumbnail = null)
    {
        $this->flyer     = $flyer;
        $this->file      = $photo;
        $this->thumbnail = $thumbnail ?: new Thumbnail;
    }

    /**
     *
     */
    public function save()
    {
        $photo = $this->flyer->addPhoto($this->makePhoto());

        $this->file->move($photo->baseDir(), $photo->name);

        $this->thumbnail->make($photo->path, $photo->thumbnail_path);

    }

    /**
     * @return Photo
     */
    private function makePhoto()
    {
        return new Photo(['name' => $this->makeFilename()]);
    }

    /**
     * @return string
     */
    private function makeFilename()
    {
        $name      = sha1(time() . $this->file->getClientOriginalName());
        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

}