<?php
/**
 * Created by PhpStorm.
 * User: iosif
 * Date: 02/11/2016
 * Time: 18:15
 */

namespace App;


use Intervention\Image\ImageManagerStatic;

class Thumbnail
{

    public function make($src, $destination)
    {
        ImageManagerStatic::make($src)
             ->fit(200)
             ->save($destination);
    }

}