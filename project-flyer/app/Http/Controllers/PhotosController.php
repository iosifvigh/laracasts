<?php

namespace App\Http\Controllers;

use App\AddPhotoToFlyerTest;
use App\Flyer;
use App\Http\Requests\AddPhotoRequest;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Requests;

class PhotosController extends Controller
{

    /**
     * Apply a photo to referenced flyer
     *
     * @param $zip
     * @param $street
     * @param AddPhotoRequest $request
     * @return string
     */
    public function store($zip, $street, AddPhotoRequest $request)
    {
        $flyer = Flyer::locatedAt($zip, $street);

        $photo = $request->file('photo');

        (new AddPhotoToFlyerTest($flyer, $photo))->save();
    }

    public function destroy($id)
    {
        Photo::findOrFail($id)->delete();

        return back();
    }
}
