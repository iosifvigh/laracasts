<?php
/**
 * Created by PhpStorm.
 * User: iosif
 * Date: 20/10/2016
 * Time: 13:23
 */

namespace App\Http\Utilities;


class Country
{
    protected static $countries = [
        'Romania' => 'ro',
        'Grand Britain' => 'gb',
        'United States' => 'us'
    ];

    public static function all()
    {
        return (static::$countries);
    }
}