<?php
/**
 * Created by PhpStorm.
 * User: iosif
 * Date: 20/10/2016
 * Time: 19:32
 */
use App\Flyer;

/**
 * @param $title
 * @param $message
 * @return mixed
 */
function flash($title = null, $message = null)
{
    $flash = app('App\Http\Flash');

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->info($title, $message);
}

/**
 * Returns the path to a given flyer
 *
 * @param Flyer $flyer
 * @return string
 */
function flyer_path(Flyer $flyer)
{
    return $flyer->zip . '/' . str_replace(' ', '-', $flyer->street);
}