<?php namespace Acme;

abstract class Sub
{
    public function make()
    {
        return $this
            ->layBread()
            ->addLettuce()
            ->addPrimaryToppings()//abstract method
            ->addSauces();
    }

    protected function layBread()
    {
        echo "\n - ";
        echo('laying down bread');

        return $this;
    }

    protected function addLettuce()
    {
        echo "\n - ";
        echo('healthy lettuce');

        return $this;
    }

    protected function addSauces()
    {
        echo "\n - ";
        echo('adding sauce');

        return $this;
    }

    /**
     * Abstract function in abstract class, cool huh?
     * This forces the classes extending this abstract class to implement this method
     * It can not be instantiated in this class
     *
     * @return mixed
     */
    protected abstract function addPrimaryToppings();
}
