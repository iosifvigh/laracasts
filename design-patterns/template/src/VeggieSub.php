<?php namespace Acme;

class VeggieSub extends Sub
{
    /**
     * @return $this
     * @deprecated
     */
    public function addVeggie()
    {
        echo "\n - ";
        echo('old veggie topping');

        return $this;
    }

    public function addPrimaryToppings()
    {
        echo "\n - ";
        echo('veggies');

        return $this;
    }
}