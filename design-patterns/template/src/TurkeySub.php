<?php namespace Acme;

class TurkeySub extends Sub
{
    /**
     * @return $this
     * @deprecated
     */
    public function addTurkey()
    {
        echo "\n - ";
        echo('old turkey topping!');

        return $this;
    }

    public function addPrimaryToppings()
    {
        echo "\n - ";
        echo('turkey!');

        return $this;
    }
}