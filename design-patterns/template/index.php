<?php
require 'vendor/autoload.php';

echo "\n\nTurkey Sub:\n";
(new Acme\TurkeySub)->make();

echo "\n\nVeggie Sub:\n";
(new Acme\VeggieSub)->make();
