<?php
/**
 * Class HomeChecker
 */
abstract class HomeChecker
{
    /**
     * This saves which class is the successor after this class.
     *
     * @var HomeChecker
     */
    protected $successor;

    /**
     * This method is checking if it can handle the request.
     * (if it fails the request must be sent down the chain of responsibility
     *
     * @param HomeStatus $home
     *
     * @return mixed
     */
    public abstract function check(HomeStatus $home);

    /**
     * Sets the object with which the home-checker will continue
     * Basically just a setter for $successor
     *
     * @param HomeChecker $successor
     */
    public function succeedWith(HomeChecker $successor)
    {
        $this->successor = $successor;
    }

    /**
     * Mechanism to call the check of the next class.
     *
     * @param HomeStatus $home
     */
    public function next(HomeStatus $home)
    {
        if ($this->successor) {
            $this->successor->check($home);
        }
    }
}

class Locks extends HomeChecker
{ //makes sure door is locked
    public function check(HomeStatus $home)
    {
        if (!$home->locked) {
            throw new Exception('Doors not locked!');
        }

        $this->next($home);
    }
}

class Lights extends HomeChecker
{ //makes sure lights are off
    public function check(HomeStatus $home)
    {
        if (!$home->lightsOff) {
            throw new Exception('Lights are not off!');
        }

        $this->next($home);
    }
}

class Alarm extends HomeChecker
{ //makes sure alarm is off
    public function check(HomeStatus $home)
    {
        if (!$home->alarmOn) {
            throw new Exception('alarm is not on!');
        }

        $this->next($home);
    }
}

class HomeStatus
{
    public $alarmOn = true;
    public $locked = true;
    public $lightsOff = true;
}

// Set objects
$locks  = new Locks;
$lights = new Lights;
$alarm  = new Alarm;

// Set chain
$locks->succeedWith($lights);
$lights->succeedWith($alarm);

// Execute chain
$locks->check(new HomeStatus);