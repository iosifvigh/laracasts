The strategy pattern enables an algorithm's behavior to be selected at runtime.

- defines a family of algorithms,
- encapsulates each algorithm, and
- makes the algorithms interchangeable within that family.
