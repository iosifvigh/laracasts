<?php namespace Acme;

/**
 * Full Class which IMPLEMENTS BookInterface and USES eReaderInterface
 * In other words, takes in an eReader and spits out a Book.
 * Adapts an eReader to a Book
 *
 * Class eReaderAdapter
 *
 * @package Acme
 */
class eReaderAdapter implements BookInterface
{

    private $reader;

    public function __construct(eReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    public function open()
    {
        return $this->reader->turnOn();
    }

    public function turnPage()
    {
        return $this->reader->pressNextButton();
    }

}