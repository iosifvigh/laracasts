"Procedural code gets information and then makes some decisions,
Object-oriented code just tells objects to do things" - Alex Sharp

As a general rule of thumb, it's best to tell your objects what to do, 
rather than asking them for their state, and then deciding what to call next, 
based upon this output. This is referred to as "Tell, Don't Ask."

example:
instead of 

if (Library->contains(Book)) {
    Library->checkOut(Book);
}

you should't interfere with the Library's logic:

Library->checkOut(Book); // Throw exception if it can't provide the book